import IsImportMeta from '@wingnut/IsImportMeta'
import Truth        from '@wingnut/Truth'

import { dirname } from 'path'
import { fileURLToPath } from 'url'



export default importMeta => {

	Truth({ importMeta }, { IsImportMeta })

	return dirname(fileURLToPath(importMeta.url))

}
